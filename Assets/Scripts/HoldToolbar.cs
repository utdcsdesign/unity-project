﻿/*
 * Master script that calls functions from other tools.
 * It uses the HoldToolWrapper on its set of tools
 * Attach this to a base object such as a floor with a collider that is assumed to be static
 * Note: the tools used in this assume a ray coming down onto the base
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class HoldToolbar : AbstractTool
{
    //Initializes all tools
    //Modify this to change functionality of toolbar
    public virtual void initTools()
    {
        List<AbstractTool> tools = new List<AbstractTool>();
        //tools.Add(gameObject.AddComponent<MoveGhostTool>());
        //tools.Add(gameObject.AddComponent<RotateTool>());
        //tools.Add(gameObject.AddComponent<ScaleTool>());
        //tools.Add(gameObject.AddComponent<DeleteTool>());
        tools.Add(gameObject.AddComponent<MineSweeperToolBase>());
        this.tools = tools.ToArray();
    }
    AbstractTool[] tools;
    GameObject[] icons;
    HoldToolWrapper ht;
    int activeIndex;
    Ray lastRay;
    bool lastRayIsTouch;
    //Used to test event with mouse
    BoardEventGenerator b;
    void Start()
    {
        setBaseID(transform.GetInstanceID());
        initTools();
        icons = new GameObject[this.tools.Length];
        for (int i = 0; i < this.icons.Length; i++)
        {
            tools[i].enabled = false;
            icons[i] = buildButton(i);
            icons[i].transform.Translate(new Vector3(GetComponent<Renderer>().bounds.extents.x * (-4.5f + i)/5, 0));
        }
        ht = gameObject.AddComponent<HoldToolWrapper>();
        activeIndex = 0;
        tools[0].enabled = true;
        ht.setTool(tools[0]);
        //Events
        b = BoardEventGenerator.getBoardEventGenerator();
        b.RayEvent += rayEvent;
    }


    /*
* Takes last received ray and calls onTouch or onHover
*/
    void Update()
    {
        if (lastRayIsTouch)
        {
            onTouch(lastRay);
        }
        else
        {
            onHover(lastRay);
        }
    }
    //calls active tool functions through the wrapper
    public override void onHover(Ray ray)
    {
        ht.onHover(ray);
    }
    public override string getName()
    {
        return "Toolbar";
    }
    //Changes selected tool or calls active tool functions through the wrapper
    public override void onTouch(Ray ray)
    {
        GameObject obj = getObject(ray);
        int index = indexIcon(obj);
        if (index >= 0)
        {
            ht.setTool(tools[index]);
            tools[activeIndex].enabled = false;
            tools[index].enabled = true;
            activeIndex = index;
            ht.onHover(ray);
        }
        else
        {
            ht.onTouch(ray);
        }
    }
    //Builds a basic button based on texture and name of tool
    private GameObject buildButton(int index)
    {
        //creates a black plane
        GameObject baseObj = GameObject.CreatePrimitive(PrimitiveType.Plane);
        baseObj.transform.parent = transform;
        baseObj.transform.localScale = new Vector3(.1f, 1, .1f);
        baseObj.transform.Translate(transform.position);
        baseObj.transform.Translate(new Vector3(0, .05f, baseObj.GetComponent<Renderer>().bounds.extents.z * 9f));
        Material mat1 = new Material(Shader.Find("Standard"));
        mat1.color = Color.black;
        //creates a plane with the tool's Texture 
        baseObj.GetComponent<Renderer>().material = mat1;
        GameObject g = GameObject.CreatePrimitive(PrimitiveType.Plane);
        g.GetComponent<Collider>().enabled = false;
        g.transform.parent = baseObj.transform;
        g.transform.position = baseObj.transform.position;
        g.transform.localScale = new Vector3(-.98f, 1, -.98f);
        g.transform.Translate(new Vector3(0, .05f, 0));
        MeshRenderer r = g.GetComponent<MeshRenderer>();
        Material mat = new Material(Shader.Find("Standard"));
        mat.mainTexture = tools[index].getDisplay();
        r.material = mat;
        //Displays the name of the tool 
        g.AddComponent<GUIText>();
        
        GameObject textObj = new GameObject();
        textObj.AddComponent<MeshRenderer>();
        TextMesh textMesh=textObj.AddComponent<TextMesh>();
        textMesh.text = tools[index].getName();
        textMesh.anchor = TextAnchor.LowerCenter;
        textObj.transform.position = g.transform.position;
        textMesh.color = Color.black;
        textObj.transform.localScale=new Vector3(.2f, -2, 1);
        textObj.transform.parent = g.transform;
        textObj.transform.Translate(new Vector3(0, 0, .5f));
        textObj.transform.Rotate( new Vector3(90, 0, 0));
        
        return baseObj;
        
    }
    /*
     * Returns the index of the icon represented in the GameObject
     */
    private int indexIcon(GameObject g)
    {
        int index = -1;
        if (g == null)
            return index;
        for(int i = 0; i < icons.Length; i++)
        {
            
            if (g.GetInstanceID() == icons[i].GetInstanceID())
            {
                return i;
            }
        }
        return index;
    }
    public override void closeTool()
    {
        foreach(GameObject icon in icons){
            icon.SetActive(false);
        }
    }
    public void rayEvent(object sender, EventArgs e)
    {
        if(e is RayEvent)
        {
            
            RayEvent r = (RayEvent)e;
            lastRayIsTouch = r.isTouch();
            lastRay = r.getRay();
        }
    }
}
