﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

public class UDPScript2 : MonoBehaviour {
	
	public Transform cube;

	static readonly object lockObject = new object();
	static UdpClient udp;
	Thread thread;
	bool dataRecieved;
	int UDPx;
	int UDPy;

	int count;
	float time;
	int currentX;
	int currentY;

	void Awake(){
	
	}

	// Use this for initialization
	void Start () {
		thread = new Thread (new ThreadStart (Search));
		thread.Start ();
	}

	void OnApplicationQuit()
	{
		udp.Close();
		thread.Abort();
	}
	
	// Update is called once per frame
	void Update () {
		time+=Time.deltaTime;
		if (dataRecieved) {
			lock (lockObject) {
				dataRecieved = false;
				currentX = UDPx;
				currentY = UDPy;
			}

			// On packet recieved code goes here
			cube.position = new Vector3(currentX, 0, currentY);
            Debug.Log(currentX + " " + currentY);
			
		}
	}
	private void Search(){
		udp = new UdpClient (2391);
		while (true)
		{
			IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

			byte[] receiveBytes = udp.Receive(ref RemoteIpEndPoint);
			string returnData = Encoding.ASCII.GetString(receiveBytes);
			string[] point = returnData.Split (' ');
			int x = int.Parse (point [0]);
			int y = int.Parse (point [1]);
			lock (lockObject){
				dataRecieved = true;
				UDPx = x / 100;
				UDPy = y / 100;

			}
			//Debug.Log (((int)(count++/time)));
		}
	}

}
