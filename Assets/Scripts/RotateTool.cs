﻿/*
 * An abstract tool extension implementing rotating
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTool : CursorTool
{
    GameObject selectedObject = null;
    GameObject objectGhost = null;
    public Material ghostMaterial;
    // Use this for initialization
    void Start()
    {
        initMaterial();
        ghostMaterial = (Material)Resources.Load("ghost");
    }
    public override string getName()
    {
        return "Rotate";
    }
    
    public override Texture getDisplay()
    {
        return Resources.Load<Texture>("rotate");
    }
    /*
     * Override of onHover to display ghost object at hover rotation
     */
    public override void onHover(Ray ray)
    {
        if (selectedObject != null)
        {
            RaycastHit hit;
            Collider c = selectedObject.GetComponent<Collider>();
            c.enabled = false;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                objectGhost.transform.rotation = selectedObject.transform.rotation;
                Vector3 dif = selectedObject.transform.position- hit.point;
                dif = new Vector3(dif.x, 0, dif.z);
                if (dif.magnitude != 0)
                {
                    double ang1 = Math.Acos(dif.x / dif.magnitude) * 180/Math.PI;
                    if(Math.Asin(dif.z / dif.magnitude) * 180 / Math.PI > 0)
                    {
                        ang1 = 360 - ang1;
                    }
                    objectGhost.transform.Rotate(new Vector3(0, (float)ang1, 0));
                }
                
            }
            c.enabled = true;
        }
        else
        {
            base.onHover(ray);
        }
    }
    /*
     * Selects and deselects objects
     */
    public override void onTouch(Ray ray){
        onHover(ray);
        base.closeTool();
        if (selectedObject==null){
            selectedObject = getObject(ray);
            if (selectedObject != null) { 
               objectGhost=(GameObject)Instantiate(selectedObject);
               objectGhost.GetComponent<Collider>().enabled=false;
                objectGhost.transform.position = selectedObject.transform.position;
               objectGhost.transform.localScale = selectedObject.transform.lossyScale;
               objectGhost.transform.lossyScale.Scale(new Vector3(1.01f,1.01f,1.01f));
               Renderer r=objectGhost.GetComponentInChildren<Renderer>();
               r.material = ghostMaterial;
            }
        }
        else
        {
            closeTool();
        }
        
    }
    public override void closeTool()
    {
        if (selectedObject != null)
        {
            selectedObject.transform.rotation = objectGhost.transform.rotation;
            selectedObject = null;
            Destroy(objectGhost);
        }
        base.closeTool();
    }
}

