﻿using System;
using System.Timers;
using UnityEngine;

/*
 * Class that creates events to be passed to the Toolbar
 * Usage involves calling getBoardEventGenerator().generateEvent(ray, isTouch)
 * If no touch event has occured in the number of milliseconds denoted by ms after
 * a touch event did occur, a hover is created using the last last event's ray.
 */
public class BoardEventGenerator
{
    //interval in millisecons in which it calls a hover event: .5 seconds
    int ms = 500;
    private static BoardEventGenerator eventGenerator;
    public event EventHandler RayEvent;
    private Timer timer;
    private Ray last;
    public static BoardEventGenerator getBoardEventGenerator()
    {
        if (eventGenerator == null)
            eventGenerator = new BoardEventGenerator();
        return eventGenerator;
    }
    private BoardEventGenerator():base()
    {
        timer = new Timer(ms);
        timer.Elapsed += onTimeout;
        timer.AutoReset = false;
        timer.Start();
    }
    protected virtual void rayEvent(EventArgs e)
    {
        if (RayEvent != null)
        {
            RayEvent(this, e);
        }
    }
    public void generateEvent(Ray r, bool touch)
    {
        timer.Stop();
        last = r;
        rayEvent(new RayEvent(r, touch));
        if (touch)
        {
            timer.Start();
        }
    }
    private void onTimeout(object sender,System.Timers.ElapsedEventArgs e)
    {
        generateEvent(last, false);
    }

}
public class RayEvent : EventArgs
{
    Ray ray;
    bool touch;
    public RayEvent(Ray r, bool touch)
    {
        ray = r;
        this.touch = touch;
    }
    public Ray getRay()
    {
        return ray;
    }
    public bool isTouch()
    {
        return touch;
    }
}