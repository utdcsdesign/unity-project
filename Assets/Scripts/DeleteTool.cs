﻿/*
 * An abstract tool extension implementing a delete function
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteTool : CursorTool
{
    
    public Material ghostMaterial;
    // Use this for initialization
    
    public override string getName()
    {
        return "Delete";
    }
    public override Texture getDisplay()
    {
        return Resources.Load<Texture>("delete");
    }
    //destroys object at ray hit
    public override void onTouch(Ray ray){
        GameObject obj = getObject(ray);
        if (obj != null)
        {
            Destroy(obj);
        }
    }
    public override void closeTool()
    {
        base.closeTool();
    }
}

