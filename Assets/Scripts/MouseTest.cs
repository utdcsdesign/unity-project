﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
/*
 * AbstractTool an object that defines actions based upon hover and touch.
 */
public class MouseTest : MonoBehaviour
{
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButton(0))
        {
            BoardEventGenerator.getBoardEventGenerator().generateEvent(ray, true);
        }
        else
            BoardEventGenerator.getBoardEventGenerator().generateEvent(ray, false);
    }
}
