﻿/*
 * An abstract tool extension implementing rotating
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleTool : CursorTool
{
    GameObject selectedObject = null;
    GameObject objectGhost = null;
    float scale=1;
    Vector3 scaleVect = new Vector3(1, 1, 1);
    public Material ghostMaterial;
    // Use this for initialization
    void Start()
    {
        initMaterial();
        ghostMaterial = (Material)Resources.Load("ghost");
    }
    public override string getName()
    {
        return "Scale";
    }
    
    public override Texture getDisplay()
    {
        return Resources.Load<Texture>("scale");
    }
    /*
     * Override of onHover to display ghost object at hover scale
     */
    public override void onHover(Ray ray)
    {
        if (selectedObject != null)
        {
            RaycastHit hit;
            Collider c = selectedObject.GetComponent<Collider>();
            c.enabled = false;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                objectGhost.transform.rotation = selectedObject.transform.rotation;
                Vector3 dif = selectedObject.transform.position- hit.point;
                dif = new Vector3(dif.x, 0, dif.z);
                if (dif.magnitude != 0)
                {
                    scale = dif.magnitude;
                    objectGhost.transform.localScale = new Vector3(scale*scaleVect.x, scale*scaleVect.y, scale*scaleVect.z);
                }
                
            }
            c.enabled = true;
        }
        else
        {
            base.onHover(ray);
        }
    }
    /*
     * Selects and deselects objects
     */
    public override void onTouch(Ray ray){
        onHover(ray);
        base.closeTool();
        if (selectedObject==null){
            selectedObject = getObject(ray);
            if (selectedObject != null) { 
               objectGhost=(GameObject)Instantiate(selectedObject);
               objectGhost.GetComponent<Collider>().enabled=false;
                objectGhost.transform.position = selectedObject.transform.position;
                scaleVect = selectedObject.transform.lossyScale;
               objectGhost.transform.localScale = selectedObject.transform.lossyScale;
               Renderer r=objectGhost.GetComponentInChildren<Renderer>();
               r.material = ghostMaterial;
            }
        }
        else
        {
            closeTool();
        }
        
    }
    public override void closeTool()
    {
        if (selectedObject != null)
        {
            selectedObject.transform.localScale = new Vector3(scale,scale,scale);
            selectedObject = null;
            scale = 1;
            Destroy(objectGhost);
        }
        base.closeTool();
    }
}

