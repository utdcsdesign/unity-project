﻿using UnityEngine;
using System.Collections;

public class InputHandler : MonoBehaviour {
    public Material highlight;

    float time = 0;
    float lastTouch = 0;

    public GameObject selected = null;

    // Update is called once per frame
    void Update () {
        time += Time.deltaTime;
        if (Input.GetMouseButtonDown(0)) {
            var position = Input.mousePosition;
            if (Time.time - lastTouch > 0.5) {
                lastTouch = Time.time;
                if (selected == null) {
                    RaycastHit hit;
                    if (Physics.Raycast(Camera.main.ScreenPointToRay(position), out hit)) {
                        //process hit
                        //select object
                        selected = hit.transform.gameObject;
                        selected.tag = "selected";

                        //add highlight material
                        MeshRenderer mr = selected.GetComponent<MeshRenderer>();
                        Material[] materials = new Material[] { mr.material, highlight };
                        mr.materials = materials;
                    }
                } else {
                    //move object
                    //Vector3 pos = selected.transform.position;
                    //selected.transform.position = pos;

                    //remove highlight material
                    MeshRenderer mr = selected.GetComponent<MeshRenderer>();
                    mr.material = mr.materials[0];

                    //deselect object
                    selected.tag = "unselected";
                    selected = null;
                }
            };
        }
	}
}
