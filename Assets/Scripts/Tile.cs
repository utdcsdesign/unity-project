﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {
    public bool isMined = false;
    public TextMesh displayText;

    [HideInInspector]
    public GameObject flag = null;
    
    [HideInInspector]
    public int x;

    [HideInInspector]
    public int y;

    void Start() {
        displayText.text = "";
        tag = "unselected";
    }
}
