﻿/*
 * An abstract tool extension changing press and hold into touch and hover
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldToolWrapper : AbstractTool
{
    
    private AbstractTool tool=null;
    private bool down=false;
    // Sets tool to be used
    public void setTool(AbstractTool newTool)
    {
        if (tool != null)
        {
            tool.closeTool();
        }
        down = false;
        tool = newTool;
    }
    public override Texture getDisplay()
    {
        if (tool == null)
            return base.getDisplay();
        return tool.getDisplay();
    }
    public override string getName()
    {
        if (tool == null)
            return "";
        return tool.getName();
    }
    /*
     * Override of onHover that calls onTouch if it was down
     */
    public override void onHover(Ray ray)
    {
        if (tool != null)
        {
            if (down)
            {
                tool.onTouch(ray);
                tool.closeTool();
                down = false;
            }
            else
            {
                tool.onHover(ray);
            }
        }
    }
    //override of onTouch that calls onHover if it was down
    public override void onTouch(Ray ray){
        if (tool != null)
        {
            if (down)
            {
                tool.onHover(ray);
            }
            else
            {
                tool.onTouch(ray);
                down = true;
            }
        }
    }
    public override void closeTool()
    {
        tool.closeTool();
        tool.enabled = false;
    }
}

