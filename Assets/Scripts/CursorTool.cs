﻿/*
 * An abstract tool extension showing where a hover is
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorTool : AbstractTool
{
    
    private static Material cursorMaterial;
    private bool cursorActive;
    private static GameObject cursor=null;
    // Use this for initialization
    void Start()
    {
        initMaterial();
    }
    //initializes curor object
    public void initMaterial()
    {
        if (cursor == null) { 
            cursorMaterial = (Material)Resources.Load("ghost");
            cursor = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            cursor.GetComponent<Collider>().enabled = false;
            cursor.GetComponent<Renderer>().material = cursorMaterial;
            cursor.transform.localScale = new Vector3(.5f, .5f, .5f);
            cursorActive = true;
        }
    }
    public override string getName()
    {
        return "Cursor";
    }
    /*
     * Override of onHover to display cursor at hover destination
     */
    public override void onHover(Ray ray)
    {
        if (cursor == null)
            initMaterial();
        if (!cursorActive)
        {
            cursor.SetActive(true);
            cursorActive = true;
        }
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            cursor.transform.position = hit.point; //Assumes origin is at bottom center of object
            cursor.transform.Translate(new Vector3(0, .1f, 0));
        }
    }
    //stops displaying the cursor
    public override void onTouch(Ray ray){
        closeTool();
    }
    public override void closeTool()
    {
        if (cursor != null)
        {
            if (cursorActive)
            {
                cursor.SetActive(false);
                cursorActive = false;
            }        
        }
    }
}

