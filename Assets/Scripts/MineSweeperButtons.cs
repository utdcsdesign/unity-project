﻿using UnityEngine;
using System.Collections;

public class MineSweeperButtons : MonoBehaviour {
    public Tile button;

    private Tile flag;
    private Tile select;
    private Tile reset;

	// Use this for initialization
	void Start () {
        flag = (Tile)Instantiate(button, new Vector3(-2.5f, 0, 3.5f), this.transform.rotation);
        flag.transform.SetParent(this.transform.parent);
        flag.displayText.text = "F";
        select = (Tile)Instantiate(button, new Vector3(-3.5f, 0, 3.5f), this.transform.rotation);
        select.transform.SetParent(this.transform.parent);
        select.displayText.text = "S";
        reset = (Tile)Instantiate(button, new Vector3(-1.5f, 0, 3.5f), this.transform.rotation);
        reset.transform.SetParent(this.transform.parent);
        reset.displayText.text = "R";
    }
	
	// Update is called once per frame
	void Update () {
	    if(flag.tag == "selected") {
            this.GetComponent<Grid>().setMode("flag");
        }
        if (select.tag == "selected") {
            this.GetComponent<Grid>().setMode("select");
        }
        if (select.tag == "reset") {
            this.GetComponent<Grid>().Reset();
        }
	}
}
