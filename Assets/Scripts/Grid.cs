﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grid: MonoBehaviour {
    public Tile tile;
    public GameObject mine;
    public GameObject flag;
    public int width = 8;
    public int height = 8;
    public float percentage = 0.15f;

    private Tile[,] tiles;
    private ArrayList unmined;
    private ArrayList mined;

    private bool initialized = false;
    private bool ended = false;

    public string mode = "select";

    // Use this for initialization
    void Start() {
        InitTiles();
    }

    public void setMode(string mode) {
        this.mode = mode;
    }

    public void Reset() {

    }

    void InitTiles() {
        tiles = new Tile[width, height];

        int cornerX = width - (width % 2 == 0 ? 1 : 0);
        int cornerZ = height - (height % 2 == 0 ? 1 : 0);
        var corner = this.transform.position - new Vector3(cornerX / 2f, 0, cornerZ / 2f);

        for (int x = 0; x < width; x++) {
            for (int z = 0; z < height; z++) {
                var newTile = (Tile)Instantiate(tile, new Vector3(corner.x + x, 0, corner.z + z - 1), this.transform.rotation);
                newTile.x = x;
                newTile.y = z;
                newTile.transform.SetParent(this.transform);
                tiles[x, z] = newTile;
            }
        }
    }

    void Update() {
        if (!ended) {
            foreach (Tile t in tiles) {
                if (t.tag == "selected") {
                    if (mode == "select") {
                        if (initialized) {
                            Reveal(t);
                        } else {
                            CreateMines(t);
                        }
                    } else if(mode == "flag") {
                        if (t.flag == null) {
                            t.flag = (GameObject) Instantiate(flag, t.transform.position + new Vector3(0, .5f), new Quaternion());
                            t.flag.transform.SetParent(this.transform.parent);
                        }
                        t.tag = "unselected";
                    }
                }
            }
        }
    }

    void CreateMines(Tile initialTile) {
        foreach (Tile t in tiles) {
            if (Random.value < percentage) {
                if (t.Equals(initialTile)) continue;
                t.isMined = true;
            }
        }
        initialized = true;
    }

    bool IsMine(int x, int y) {
        if (x >= 0 && y >= 0 && x < width && y < height)
            return tiles[x, y].isMined;
        return false;
    }

    void Reveal(Tile t) {
        if (t.isMined) {
            RevealMines();
        } else {
            if(t.flag != null) {
                Destroy(t.flag);
                t.flag = null;
            }
            DisplayText(t);
            FloodFill(t.x, t.y, new bool[width, height]);
        }
    }

    void DisplayText(Tile t) {
        int count = GetAdjacentCount(t.x, t.y);
        t.displayText.text = count.ToString();
        if (count == 0) {
            t.displayText.color = Color.white;
        } else {
            t.displayText.color = Color.Lerp(Color.green, Color.red, count / 8f);
        }
    }

    void RevealMines() {
        if (!ended) {
            foreach (Tile t in tiles) {
                if (t.isMined) {
                    GameObject m = (GameObject) Instantiate(mine, new Vector3(t.transform.position.x, t.transform.position.y + 1, t.transform.position.z), this.transform.rotation);
                    m.transform.SetParent(this.transform);
                }
            }
            ended = true;
        }
    }

    int GetAdjacentCount(int x, int y) {
        int count = 0;

        if (IsMine(x, y + 1)) ++count; // top
        if (IsMine(x + 1, y + 1)) ++count; // top-right
        if (IsMine(x + 1, y)) ++count; // right
        if (IsMine(x + 1, y - 1)) ++count; // bottom-right
        if (IsMine(x, y - 1)) ++count; // bottom
        if (IsMine(x - 1, y - 1)) ++count; // bottom-left
        if (IsMine(x - 1, y)) ++count; // left
        if (IsMine(x - 1, y + 1)) ++count; // top-left

        return count;
    }

    void FloodFill(int x, int y, bool[,] visited) {
        if (x >= 0 && y >= 0 && x < width && y < height) {
            if (visited[x, y]) {
                return;
            }

            DisplayText(tiles[x, y]);

            if (GetAdjacentCount(x, y) > 0) {
                return;
            }

            visited[x, y] = true;

            FloodFill(x - 1, y, visited);
            FloodFill(x + 1, y, visited);
            FloodFill(x, y - 1, visited);
            FloodFill(x, y + 1, visited);
            FloodFill(x - 1, y - 1, visited);
            FloodFill(x - 1, y + 1, visited);
            FloodFill(x + 1, y + 1, visited);
            FloodFill(x + 1, y - 1, visited);
        }
    }
}
