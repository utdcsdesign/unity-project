﻿/*
 * An abstract tool extension that will show where the cursor is if held down.
 * Fill in the functions: getName, getDisplay, and toolFunction.
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineSweeperToolBase : CursorTool
{
    //Returns text to be displayed on the button
    public override string getName()
    {
        return "";
    }
    //Returns texture image to be displayed on the button
    public override Texture getDisplay()
    {
        //example implementation
        return Resources.Load<Texture>("move");
    }
    //Do tool actions here
    //Provides the ray given as well as the GameObject pointed to by the ray
    public virtual void toolFunction(GameObject obj, Ray r)
    {

    }
    GameObject selectedObject = null;//GameObject that is found at selections
    
    /*
     * Selects and deselects object at ray
     */
    public override void onTouch(Ray ray){
        onHover(ray);
        base.closeTool();
        if (selectedObject==null){
            selectedObject = getObject(ray);
        }
        else
        {
            if(selectedObject.GetInstanceID()==getObject(ray).GetInstanceID())
                toolFunction(selectedObject, ray);
            selectedObject = null;
        }
        
    }
    public override void closeTool()
    {
        selectedObject = null;
        base.closeTool();
    }
}

