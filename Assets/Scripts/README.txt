To use these files:
Place them into the assets folder in the unity project.
Attach the HoldToolbar or extention to base object.
	For extending with different functionality, change the initTools function to add the desired tools.
	
Use the BoardEvent object to send events to the toolbar. (An example of use is in MouseTest.cs)
	If mouse actions should affect the project, attach the MouseTest script to the base object.
(Note:the rays used by the current set of tools work for rays off the base object pointing towards it.)

New tools:
	Extend an existing tool with functions.
	In most cases, ToolBase will perform desired actions by overriding the 
	toolFunction, getName and getDisplay methods.
	When used by a HoldToolbar, the toolFunction will be called if the object pointed to at the
	beginning and end of the touch is the same.

