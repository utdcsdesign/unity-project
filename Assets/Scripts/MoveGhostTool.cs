﻿/*
 * An abstract tool extension that will select an object and show
 * where it will end up.
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveGhostTool : CursorTool
{
    GameObject selectedObject = null;
    GameObject objectGhost = null;
    public Material ghostMaterial;
    // Use this for initialization
    void Start()
    {
        initMaterial();
        ghostMaterial = (Material)Resources.Load("ghost");
    }
    public override string getName()
    {
        return "Move";
    }
    public override Texture getDisplay()
    {
        return Resources.Load<Texture>("move");
    }
    /*
     * Override of onHover to display ghost object at hover destination
     */
    public override void onHover(Ray ray)
    {
        if (selectedObject != null)
        {
            RaycastHit hit;
            Collider c = selectedObject.GetComponent<Collider>();
            c.enabled = false;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                objectGhost.transform.position = hit.point; //Assumes origin is at bottom center of object
                objectGhost.transform.Translate(new Vector3(0, .1f, 0));
            }
            c.enabled = true;
        }
        else
        {
            base.onHover(ray);
        }
    }
    /*
     * Selects and deselects object at ray
     */
    public override void onTouch(Ray ray){
        base.closeTool();
        if (selectedObject==null){
            selectedObject = getObject(ray);
            if (selectedObject != null) { 
               objectGhost=(GameObject)Instantiate(selectedObject);
               objectGhost.GetComponent<Collider>().enabled=false;
               onHover(ray);
               objectGhost.transform.localScale = selectedObject.transform.lossyScale;
               objectGhost.transform.lossyScale.Scale(new Vector3(1.01f,1.01f,1.01f));
               Renderer r=objectGhost.GetComponentInChildren<Renderer>();
               r.material = ghostMaterial;
            }
        }
        else
        {
            onHover(ray);
            closeTool();
        }
        
    }
    public override void closeTool()
    {
        if (selectedObject != null)
        {
            selectedObject.transform.position = objectGhost.transform.position;
            selectedObject = null;
            Destroy(objectGhost);
        }
        base.closeTool();
    }
}

