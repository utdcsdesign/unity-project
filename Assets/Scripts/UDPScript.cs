﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System;

public class UDPScript : MonoBehaviour {
    public Material highlight;
    public int port;

    private GameObject selected = null;

	static readonly object lockObject = new object();
	static UdpClient udp;
	Thread thread;
	bool dataRecieved;
	int UDPx;
	int UDPy;

	int count;
	float time;
    float lastTouch = 0;
    Vector3 position;

	void Awake(){
		
	}

	// Use this for initialization
	void Start () {
		thread = new Thread (new ThreadStart (Search));
		thread.Start ();
        position = new Vector3();
	}

	void OnApplicationQuit()
	{
		udp.Close();
		thread.Abort();
	}
	
	// Update is called once per frame
	void Update () {
		time+=Time.deltaTime;
		if (dataRecieved) {
			lock (lockObject) {
				dataRecieved = false;
               
				position.x = UDPx;
                position.y = 0;
                position.z = UDPy;
			}
            Debug.Log("x: " + position.x + " y: 0 z: " + position.z);
            // On packet recieved code goes here
            if (Time.time - lastTouch > 0.5) {
                lastTouch = Time.time;

                if (selected == null) {
                    RaycastHit hit;
                    if (Physics.Raycast(position, Vector3.up, out hit)) {
                        //process hit
                        //select object
                        selected = hit.transform.gameObject;
                        selected.tag = "selected";
                        selected.GetComponent<MeshRenderer>().material.color = Color.blue;

                        //add highlight material
                        //MeshRenderer mr = selected.GetComponent<MeshRenderer>();
                        //Material[] materials = new Material[] { mr.material, highlight };
                        //mr.materials = materials;
                    }
                } else {
                    //move object
                    //Vector3 pos = selected.transform.position;
                    //selected.transform.position = pos;

                    //remove highlight material
                    //MeshRenderer mr = selected.GetComponent<MeshRenderer>();
                    //mr.material = mr.materials[0];

                    //deselect object
                    selected.tag = "unselected";
                    selected.GetComponent<MeshRenderer>().material.color = Color.white;
                    selected = null;
                }
            }
		}
	}
	private void Search(){
		udp = new UdpClient (port);
		while (true)
		{
			IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

			byte[] receiveBytes = udp.Receive(ref RemoteIpEndPoint);
			string returnData = Encoding.ASCII.GetString(receiveBytes);
			string[] point = returnData.Split (' ');
			int x = int.Parse (point [0]);
			int y = int.Parse (point [1]);
			lock (lockObject){
				dataRecieved = true;
				UDPx = x;
				UDPy = y;

			}
			//Debug.Log (((int)(count++/time)));
		}
	}

}
