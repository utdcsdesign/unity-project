﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
/*
 * AbstractTool an object that defines actions based upon hover and touch.
 */
public abstract class AbstractTool : MonoBehaviour
{
    private static int baseInstanceId=0;
    
    public void setBaseID(int id)
    {
        if (baseInstanceId == 0)
        {
            baseInstanceId = id;
        }
    }
    public int getBaseID()
    {
        return baseInstanceId;
    }
    /*
     * Action of tool based on hover.
     * This is mostly for displaying what would happen if a touch occurs
     */
    public abstract void onHover(Ray ray);
    /*
     * Action of tool based on touch.
     * This function results in changes to the object.
     */
    public abstract void onTouch(Ray ray);
    /*
     * Method to get the texture of the object.
     */
    public virtual Texture getDisplay()
    {
        return Resources.Load<Texture>("question");
    }
    /*
     * Brings the tool to a default state that no longer displayes tool-specific objects
     */
    public abstract void closeTool();
    /*
     * Method returns a defined name to be displayed.
     */
    public abstract string getName();
    /*
     * Method that returns the first object the Ray would colide with.
     * This ignores the base object.
     * If no object would be hit, it returns null.
     */
    public GameObject getObject(Ray ray)
    {
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            if (hit.transform.GetInstanceID() != baseInstanceId)//ignores the base object
            {
                return hit.transform.gameObject;
            }
        }
        return null;
    }
}
